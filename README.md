# Launchbox CircleCI Docker Images

This repository contains custom docker images to use in CircleCI.

## Images

### node

Includes:
* node
* yarn
* aws-cli
* eb-cli

### postgres

An extended postgres image that comes with pre-created databases for testing.

## How it works

Pushing commits and tags to BitBucket will result in a public bsdlaunchbox Docker image being built automatically by [Docker Hub](https://hub.docker.com/u/bsdsystems).

## Deploying new versions

Use git tags to define the versions of the Docker image:
`git tag -a node/v1 -m "Version 1 release"`

Preferably, add the tag before you push the commit, but if you have already pushed the commit then use `git push origin node/v1`.
